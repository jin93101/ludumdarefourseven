﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static float score;
    public static bool gameOver = false;
    public static float globalTimer;

    public float scoreBase = 0;

    public float currencyCounter;

    public Text displayScoreText;
    public Text displayHealthText;

    public Text gameOverText;
    public Text finalScore;

    public GameObject gameOverScreen;
    public GameObject gameUI;

    private void Start()
    {
        score = scoreBase;
        gameOverScreen.SetActive(false);
        gameUI.SetActive(true);
    }

    void Update()
    {
        globalTimer += Time.deltaTime;

        displayScoreText.text = "Score : " + score;
        displayHealthText.text = "Health : " + Player.health;

        //checking if player is still alive
        if (Player.health <= 0)
        {
            gameOver = true;
        }
        else
        {
            gameOver = false;
        }

        //display final screen text
        if(gameOver == true)
        {
            //display correct text on the screen
            gameOverScreen.SetActive(true);
            gameUI.SetActive(false);
            //instructions for restart
            gameOverText.text = "This is game over! Press [Enter] to play again.";
            finalScore.text = "Your final score is : " + GameManager.score;

            if (Input.GetKeyDown(KeyCode.Return))
            {
                globalTimer = 0;
                Player.health = 3;
                gameOver = false;
                string currentSceneName = SceneManager.GetActiveScene().name;
                SceneManager.LoadScene(currentSceneName);
            }
        }
    }
}
