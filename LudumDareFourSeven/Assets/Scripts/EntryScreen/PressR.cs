﻿using UnityEngine;

public class PressR : MonoBehaviour
{
    public Animator anim;
    public int counter = 0;
    public GameObject newGame;
    public GameObject title;
    public GameObject toRotate;

    private void Update()
    {
        if (Input.GetKeyDown("r"))
        {
            counter++;
            anim.SetBool("isActive", true);
        }
        else
        {
            anim.SetBool("isActive", false);
        }

        if(counter == 1)
        {
            Instantiate(toRotate,new Vector2(2.8f,-2.08f), Quaternion.identity);
            counter = 2;
        }else if (counter == 3)
        {
            Instantiate(title, new Vector2(0, 3.53f), Quaternion.identity);
            counter = 4;
        }
        else if (counter == 5)
        {
            Instantiate(newGame, new Vector2(0.5f, -2.48f), Quaternion.identity);
            counter = 6;
        }
    }
}
