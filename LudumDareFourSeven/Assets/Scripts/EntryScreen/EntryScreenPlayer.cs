﻿using UnityEngine;

public class EntryScreenPlayer : MonoBehaviour
{
    public float x;
    public float y;

    public float angle = 0f;
    public float speed = 1f;
    public float radius = 1f;

    private bool _leftRotation = true;

    //To Do
    public static int health = 3;
    public float speedChange;
    public float radiusChange;

    public static bool imHit = false;

    public float timer;
    public float baseTimer = 0;

    private void Update()
    {
        x = Mathf.Cos(angle) * radius;
        y = Mathf.Sin(angle) * radius;

        // basic rotation
        transform.position = new Vector2(x, y);
        if (_leftRotation == true)
        {
            angle += speed * Time.deltaTime;
        }
        else if (_leftRotation == false)
        {
            angle -= speed * Time.deltaTime;
        }

        //change rotation direction
        if (Input.GetKeyDown("r"))
        {
            if (_leftRotation == true)
            {
                _leftRotation = false;
            }
            else if (_leftRotation == false)
            {
                _leftRotation = true;
            }
        }

        //health
        if (health <= 0)
        {
            health = 0;
        }

        if (health > 3)
        {
            health = 3;
        }

        if (imHit == true)
        {
            timer += Time.deltaTime;
            if (timer >= 1)
            {
                imHit = false;
                timer = baseTimer;
            }
        }

        //TO DO
        //change speed rotation
        //change rotation radius
        //slow down time

    }
}
