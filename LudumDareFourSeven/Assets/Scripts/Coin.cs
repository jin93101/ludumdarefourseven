﻿using UnityEngine;

public class Coin : MonoBehaviour
{
    private float _speed = 3f;
    public GameObject sound;
    public GameObject coinPicked;

    private void Update()
    {
        if (GameManager.gameOver != true)
        {
            transform.Translate(Vector2.left * _speed * Time.deltaTime);
        }

        if (transform.position.x < -9)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Instantiate(coinPicked, new Vector2(transform.position.x, transform.position.y + 1f), Quaternion.identity);
            Instantiate(sound, transform.position, Quaternion.identity);
            GameManager.score += 10;
            Destroy(gameObject);
        }
    }
}
