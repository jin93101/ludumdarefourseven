﻿using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public GameObject enemyObject;
    void Start()
    {
        Instantiate(enemyObject, transform.position, Quaternion.identity);
    }

}
