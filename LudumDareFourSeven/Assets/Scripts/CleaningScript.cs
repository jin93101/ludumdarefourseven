﻿using UnityEngine;

public class CleaningScript : MonoBehaviour
{
    private float killMe = 2;

    private void Update()
    {
        killMe -= Time.deltaTime;
        if(killMe <= 0)
        {
            Destroy(gameObject);
            return;
        }
    }
}
