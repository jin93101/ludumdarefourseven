﻿using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if(Player.imHit == true)
        {
            anim.SetBool("isActive", true);
        }
        else if(Player.imHit == false)
        {
            anim.SetBool("isActive", false);
        } 
    }
}
