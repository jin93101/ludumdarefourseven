﻿using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    public GameObject[] coinCollection;

    private float _timeBtwSpawns;
    public float startTimeBtwSpawns;


    private void Update()
    {
        //spawn only if the game is still on
        if ((GameManager.gameOver != true) && (GameManager.globalTimer > 12))
        {
            if (_timeBtwSpawns <= 0)
            {
                int rand = Random.Range(0, coinCollection.Length);
                Instantiate(coinCollection[rand], transform.position, Quaternion.identity);
                _timeBtwSpawns = startTimeBtwSpawns;
            }
            else
            {
                _timeBtwSpawns -= Time.deltaTime;
            }
        }
    }
}
