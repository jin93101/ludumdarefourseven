﻿using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] spawnCollection;

    private float _timeBtwSpawns;
    public float startTimeBtwSpawns;


    private void Update()
    {
        if(GameManager.globalTimer > 60)
        {
            startTimeBtwSpawns = 1;
        }

        //spawn only if the game is still on
        if (GameManager.gameOver != true)
        {
            if (_timeBtwSpawns <= 0)
            {
                int rand = Random.Range(0, spawnCollection.Length);
                Instantiate(spawnCollection[rand], transform.position, Quaternion.identity);
                _timeBtwSpawns = startTimeBtwSpawns;
            }
            else
            {
                _timeBtwSpawns -= Time.deltaTime;
            }
        }
    }
}
