﻿using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    public Sprite threeHearts;
    public Sprite twoHearts;
    public Sprite oneHeart;
    public Sprite emptyHearts;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (Player.health == 3)
        {
            spriteRenderer.sprite = threeHearts;
        }else if (Player.health == 2)
        {
            spriteRenderer.sprite = twoHearts;
        }
        else if (Player.health == 1)
        {
            spriteRenderer.sprite = oneHeart;
        }
        else if (Player.health == 0)
        {
            spriteRenderer.sprite = emptyHearts;
        }
    }
}
