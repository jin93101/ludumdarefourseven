﻿using UnityEngine;

public class SoundController : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip[] soundCollection;
    public int rand;

    public AudioClip gameOverTunes;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (GameManager.gameOver == false)
        {
            if (!audioSource.isPlaying)
            {
                rand = Random.Range(0, soundCollection.Length);
                audioSource.clip = soundCollection[rand];
                audioSource.Play();
            }
        }
        else
        {
            audioSource.Stop();
        }   
    }
}
