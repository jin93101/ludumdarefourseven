﻿using UnityEngine;

public class Health : MonoBehaviour
{
    public GameObject sound;
    public GameObject heartPicked;

    private void Update()
    {
        if (GameManager.gameOver != true)
        {
            transform.Translate(Vector2.left * Time.deltaTime);
        }
            
        if (transform.position.x < -9)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Instantiate(heartPicked, new Vector2(transform.position.x, transform.position.y + 1f), Quaternion.identity);
            Instantiate(sound, transform.position, Quaternion.identity);
            GameManager.score += 3;
            Player.health += 1;
            Destroy(gameObject);
        }
    }
}
