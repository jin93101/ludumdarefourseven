﻿using UnityEngine;

public class GameOverSounds : MonoBehaviour
{
    public GameObject[] soundCollection;

    public float timeBtwSpawn;
    public float startTime = 3f;
    private int _rand;

    private void Update()
    {
        if (timeBtwSpawn <= 0)
        {
            _rand = Random.Range(0, soundCollection.Length);
            Instantiate(soundCollection[_rand], transform.position, Quaternion.identity);
            timeBtwSpawn = startTime;
        }
        else
        {
            timeBtwSpawn -= Time.deltaTime;
        }

        if(GameManager.gameOver == false)
        {
            Destroy(gameObject);
            return;
        }
    }
}
