﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public float x;
    public float y;

    /*
     142, 71, 140 = 8e478c purple   0.56f, 0.28f, 0.55f, 1
     57, 120, 168 = 3978a8 blue     0.22f, 0.47f, 0.66f, 1
     57, 123, 68 = 397b44 green     0.22f, 0.48f, 0.27f, 1

    dark colors

    dark purple 57, 49, 75 = 0.22f, 0.19f, 0.29f, 1
    dark green 60, 89, 86 = 0.24f, 0.35f, 0.34f, 1
    dark blue 57, 71, 120 = 0.22f, 0.28f, 0.47f, 1

     */

    public static int newColorWeUse;

    public float rand;

    public float angle = 0f;
    public float speed = 1f;
    public float radius = 1f;

    private bool _leftRotation = true;
    private SpriteRenderer spriteRenderer;
    private TrailRenderer trailRenderer;

    public GameObject[] changeRotationSound;
    public GameObject deathAnimation;
    public GameObject pressedR;

    //To Do
    public static int health = 3;
    public float speedChange;
    public float radiusChange;

    public static bool imHit = false;

    public float timer;
    public float baseTimer = 0;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        trailRenderer = GetComponent<TrailRenderer>();

        rand = Random.Range(0, 3);
        switch (rand)
        {
            case 1:
                newColorWeUse = 1;
                //green
                spriteRenderer.color = new Color(0.22f, 0.48f, 0.27f, 1);
                trailRenderer.startColor = new Color(0.22f, 0.48f, 0.27f, 1);
                trailRenderer.endColor = new Color(1, 1, 1, 1);
                break;

            case 2:
                newColorWeUse = 2;
                //blue
                spriteRenderer.color = new Color(0.22f, 0.47f, 0.66f, 1);
                trailRenderer.startColor = new Color(0.22f, 0.47f, 0.66f, 1);
                trailRenderer.endColor = new Color(1, 1, 1, 1);
                break;

            case 3:
                newColorWeUse = 3;
                //purple
                spriteRenderer.color = new Color(0.56f, 0.28f, 0.55f, 1);
                trailRenderer.startColor = new Color(0.56f, 0.28f, 0.55f, 1);
                trailRenderer.endColor = new Color(1, 1, 1, 1);
                break;

            default:
                newColorWeUse = 3;
                spriteRenderer.color = new Color(0.56f, 0.28f, 0.55f, 1);
                trailRenderer.startColor = new Color(0.56f, 0.28f, 0.55f, 1);
                trailRenderer.endColor = new Color(1, 1, 1, 1);
                break;

        }
    }

    private void Update()
    {
        x = Mathf.Cos(angle) * radius;
        y = Mathf.Sin(angle) * radius;

        // basic rotation
        transform.position = new Vector2(x, y);
        if (_leftRotation == true)
        {
            angle += speed * Time.deltaTime;
        }
        else if (_leftRotation == false)
        {
            angle -= speed * Time.deltaTime;
        }

        //change rotation direction
        if (Input.GetKeyDown("r"))
        {
            //add random sounds here
            int rand = Random.Range(0, changeRotationSound.Length);
            Instantiate(changeRotationSound[rand], transform.position, Quaternion.identity);
            //press effect
            Instantiate(pressedR, transform.position, Quaternion.identity);

            if (_leftRotation == true)
            {
                _leftRotation = false;
            }
            else if (_leftRotation == false)
            {
                _leftRotation = true;
            }
        }

        if (imHit == true)
        {
            timer += Time.deltaTime;
            if (timer >= 1)
            {
                imHit = false;
                timer = baseTimer;
            }
        }
        //health
        if (health <=0)
        {
            health = 0;
            Instantiate(deathAnimation, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

        if(health > 3)
        {
            health = 3;
        }

        if (newColorWeUse == 1)
        {
            //green
            spriteRenderer.color = new Color(0.22f, 0.48f, 0.27f, 1);
            trailRenderer.startColor = new Color(0.22f, 0.48f, 0.27f, 1);
            trailRenderer.endColor = new Color(1, 1, 1, 1);
        }
        else if (newColorWeUse == 2)
        {
            //blue
            spriteRenderer.color = new Color(0.22f, 0.47f, 0.66f, 1);
            trailRenderer.startColor = new Color(0.22f, 0.47f, 0.66f, 1);
            trailRenderer.endColor = new Color(1, 1, 1, 1);
        }
        else if (newColorWeUse == 3)
        {
            //purple
            spriteRenderer.color = new Color(0.56f, 0.28f, 0.55f, 1);
            trailRenderer.startColor = new Color(0.56f, 0.28f, 0.55f, 1);
            trailRenderer.endColor = new Color(1, 1, 1, 1);
        }
    }
}
