﻿using UnityEngine;

public class BeatMotion : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;

    public float speed = 0.5f;

    public float endX = -15.4f;
    public float startX = 23.4f;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {

        if (Player.newColorWeUse == 1)
        {
            //dark green
            //spriteRenderer.color = new Color(0.24f, 0.35f, 0.34f, 0.31f);
            //green
            spriteRenderer.color = new Color(0.22f, 0.48f, 0.27f, 0.31f);
        }
        else if(Player.newColorWeUse == 2)
        {
            //dark blue
            //spriteRenderer.color = new Color(0.22f, 0.28f, 0.47f, 0.31f);
            //blue
            spriteRenderer.color = new Color(0.22f, 0.47f, 0.66f, 0.31f);
        }
        else if(Player.newColorWeUse == 3)
        {
            //dark purple
            //spriteRenderer.color = new Color(0.22f, 0.19f, 0.29f, 0.31f);
            //purple
            spriteRenderer.color = new Color(0.56f, 0.28f, 0.55f, 0.31f);
        }

        transform.Translate(Vector2.left * speed * Time.deltaTime);
    
        if(transform.position.x <= endX)
        {
            Vector2 pos = new Vector2(startX, transform.position.y);
            transform.position = pos;
        }
    }
}
