﻿using UnityEngine;

public class HeartSpawner : MonoBehaviour
{
    public GameObject[] heartCollection;

    private float _timeBtwSpawns;
    public float startTimeBtwSpawns;


    private void Update()
    {
     //spawn only if the game is still on
        if ((GameManager.gameOver != true) && (Player.health < 3))
        {
            if (_timeBtwSpawns <= 0)
            {
                int rand = Random.Range(0, heartCollection.Length);
                Instantiate(heartCollection[rand], transform.position, Quaternion.identity);
                _timeBtwSpawns = startTimeBtwSpawns;
            }
            else
            {
                _timeBtwSpawns -= Time.deltaTime;
            }
        }
    }
}
