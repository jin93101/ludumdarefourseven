﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public static int nextLevelIndex = 0;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            audioSource.Stop();
            nextLevelIndex++;
            string nextLevelName = "Level" + nextLevelIndex;
            SceneManager.LoadScene(nextLevelName);
        }
    }
}
