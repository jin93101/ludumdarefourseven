﻿using UnityEngine;

public class EnemyObject : MonoBehaviour
{
    private float _speed;
    public float basicSpeed = 2f;

    public GameObject smoko;
    public GameObject sound;

    private SpriteRenderer spriteRenderer;

    //speed "management"
    private float _firstStage = 60;
    private float _secondStage = 80;
    private float _thirdStage = 100;
    private float _fourthStage = 120;
    private float _fifthStage = 140;
    private float _lastStage = 160;

    private void Start()
    {
        _speed = basicSpeed;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {

        if (Player.newColorWeUse == 1)
        {
            //green
            spriteRenderer.color = new Color(0.22f, 0.48f, 0.27f, 1);
        }
        else if (Player.newColorWeUse == 2)
        {
            //blue
            spriteRenderer.color = new Color(0.22f, 0.47f, 0.66f, 1);
        }
        else if (Player.newColorWeUse == 3)
        {
            //purple
            spriteRenderer.color = new Color(0.56f, 0.28f, 0.55f, 1);
        }


        //stop the movement with game over
        if (GameManager.gameOver == true)
        {
            _speed = 0;
        } else if(GameManager.gameOver == false)
        {
            transform.Translate(Vector2.left * _speed * Time.deltaTime);

            //speeding up - hard coded
            if ((GameManager.globalTimer >= _firstStage) && (GameManager.globalTimer < _secondStage))
            {
                transform.Translate(Vector2.left * 2.1f * Time.deltaTime);
                Player.newColorWeUse = 1;

            }
            else if ((GameManager.globalTimer >= _secondStage) && (GameManager.globalTimer < _thirdStage))
            {
                transform.Translate(Vector2.left * 2.2f * Time.deltaTime);
                Player.newColorWeUse = 2;

            }
            else if ((GameManager.globalTimer >= _thirdStage) && (GameManager.globalTimer < _fourthStage))
            {
                transform.Translate(Vector2.left * 2.3f * Time.deltaTime);
                Player.newColorWeUse = 3;

            }
            else if ((GameManager.globalTimer >= _fourthStage) && (GameManager.globalTimer < _fifthStage))
            {
                transform.Translate(Vector2.left * 2.4f * Time.deltaTime);
                Player.newColorWeUse = 1;

            }
            else if ((GameManager.globalTimer >= _fifthStage) && (GameManager.globalTimer < _lastStage))
            {
                transform.Translate(Vector2.left * 2.5f * Time.deltaTime);
                Player.newColorWeUse = 2;

            }
            else if (GameManager.globalTimer >= _lastStage)
            {
                transform.Translate(Vector2.left * 3f * Time.deltaTime);
                Player.newColorWeUse = 3;

            }
        }

        //selfdestroy and add score
        if (transform.position.x < -9)
        {
            GameManager.score += 1;
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Instantiate(sound, transform.position, Quaternion.identity);
            Player.imHit = true;
            Player.health -= 1;
            Instantiate(smoko, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
